package com.epam;

import java.util.Scanner;
public class Calculator {
  public static void main(String[] args) {
    Scanner reader = new Scanner(System.in);
    System.out.print("Enter two numbers:");
    double a=reader.nextDouble();
    double b=reader.nextDouble();
    double result;
    System.out.print("Enter operator:");
     
    char op=reader.next().charAt(0);
    switch (op) {
      case '+':
        result = a+b;
        break;
      case '-':
        result = a-b;
        break;
      case '*':
        result = a*b;
        break;
      case '/':
        result = a/b;
        break;
      default:
        System.out.println("invalid operator");
        reader.close();
        return;
    }
    System.out.println(result);
    reader.close();
 }
}